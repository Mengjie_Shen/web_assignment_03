-- Solutions to question 2 here

-- A. Who won the world cup final?
SELECT winning_team
FROM hwtwo_games
WHERE game_type = 'Final';

-- B. List the winning teams from the Quarterfinal, Semifinal, Third-Place, and the Final
SELECT DISTINCT winning_team
FROM hwtwo_games
WHERE game_type IN ('Quarterfinal', 'Semifinal', 'Third-Place', 'Final');

-- C. How many teams in total competed in the tournament?
SELECT DISTINCT team1_name
FROM hwtwo_games
UNION
SELECT DISTINCT team2_name
FROM hwtwo_games;

-- D. List the teams that had one or more red cards assigned to them
SELECT team_name, sum(red_cards) AS red_card_total
FROM hwtwo_players
GROUP BY team_name
HAVING sum(red_cards) >= 1;

-- E. List the players that scored more than 2 goals in the tournament (displaying the
-- player’s name, and how many goals they scored). Order the list by goals scored,
-- highest to lowest
SELECT player_name, goals_scored
FROM hwtwo_players
WHERE goals_scored > 2
ORDER BY goals_scored DESC;


-- F. The same question as posed in (E) above, but with the team name and ‘team shots
-- on goal’ also displayed in the result-set
SELECT player_name, goals_scored, hwtwo_players.team_name AS team_name, hwtwo_teams.team_shots_on_goal AS team_shots_on_goal
FROM hwtwo_players
LEFT JOIN hwtwo_teams
ON hwtwo_players.team_name = hwtwo_teams.team_name
WHERE goals_scored > 2
ORDER BY goals_scored DESC;


-- G. List the winning team of each match from ‘Group A’ matches (6 matches were
-- played, so the resulting table should have 6 rows to it)
SELECT game_id, winning_team, game_type
FROM hwtwo_games
WHERE game_type = 'Group A';
