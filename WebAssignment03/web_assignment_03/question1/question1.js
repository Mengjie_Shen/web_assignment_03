/* Develop your answer to Question 2 here */
"use strict";


var current_banner_num = 1;

function crossfade() {
    $("#banner-" + current_banner_num).fadeOut();

    if (current_banner_num == 4) {
        current_banner_num = 1;
    } else {
        current_banner_num++;
    }
    $("#banner-" + current_banner_num).fadeIn();
}

var intervalTimer = null;
var isOn = false;


$(document).ready(function () {

    $("#banner-base").css("visibility", "hidden"); // Use hidden to keep width and height of div same as image
    $("#banner-" + current_banner_num).show();

    /*    var next_button = $("#next").button();

        next_button.click(function() {

        $("#banner-"+current_banner_num).hide();

        if (current_banner_num==4) {
            current_banner_num = 1;
        }
        else {
            current_banner_num++;
        }

        $("#banner-"+current_banner_num).show();

        });*/

    $("#slider").slider();
    $("#slider").slider("option", "min", 1);
    $("#slider").slider("option", "max", 3);
    $("#slider").slider("option", "value", 1);

    $("#slider").slider({
            change: function () {
                var value = $("#slider").slider("option", "value");
                if (isOn && window.intervalTimer) {
                    clearInterval(window.intervalTimer);
                    window.intervalTimer = setInterval(crossfade, 5000 / value);
                    console.log("slider: " + $("#slider").slider("option", "value"));
                    console.log("slider: " + window.intervalTimer);
                }
            }
        }
    );

    var start_button = $("#start").button();
    start_button.click(function () {
        if (window.intervalTimer) {
            clearInterval(window.intervalTimer);
        }
        var value = $("#slider").slider("option", "value");
        window.intervalTimer = setInterval(crossfade, 5000 / value);
        isOn = true;
        console.log("start: " + window.intervalTimer);
    });

    var stop_button = $("#stop").button();
    stop_button.click(function () {
        clearInterval(window.intervalTimer);
        window.intervalTimer = null;
        isOn = false;
        console.log("stop: " + window.intervalTimer);
    });


});
		    
